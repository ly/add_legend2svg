# -*- coding:UTF-8 -*-

import os
import re
import sys
from argparse import ArgumentParser
p = ArgumentParser()
p.add_argument('-i','--insvg',help='输入circos输入的svg，建议版本 circos v0.69.6')
p.add_argument('-o','--outputdir',help='结果的输出目录')
p.add_argument('-p','--prefix',help='输入的prefix')
p.add_argument('-c','--conf',help='输入配置文件')


def run(insvg,outdir,prefix,conf):


	import json,os,re
	with open(conf,'r') as f:
		config_dict = json.load(f)

	
	if "legend_rect_x" not in config_dict:
		config_dict['legend_rect_x'] = 0.87

	if "legend_rectyx" not in config_dict:
		config_dict['legend_rect_y'] = 0.8

	if "new_svg_width" not in config_dict:
	
		config_dict['new_svg_width'] = 1.3
	if "new_svg_heigth" not in config_dict:
		config_dict['new_svg_height'] = 1.1

	if "rect_width" not in config_dict:

			config_dict['rect_width'] = 0.22

	
	#--- 计算增加图例的个数 ---
	tmp_count = 0
	tmp_count = int(tmp_count)
	script = ""

	#--- 开始添加legend
	#--- 拷贝 svg 到工作目录
	os.system("cp %s %s/%s.svg" % (insvg,outdir,prefix))
	#--- 获取circos svg 的长度和宽度信息
	line3 = os.popen("cat %s | head -3|tail -1" % insvg).read()
	circos_width = line3.split('svg width=\"')[1].split('px\" height=\"')[0]
	circos_height = line3.split(' height=\"')[1].split('px\" version=')[0]

	circos_width = int(circos_width)
	circos_height = int(circos_height)
	
	new_circos_width = circos_width *  config_dict['new_svg_width']
	new_circos_height = circos_height * config_dict['new_svg_height']
		
	#--- 增加图例的脚本 ---
	#--- 先画一个矩形
	rect_width = circos_width * config_dict['rect_width']
	rect_x = circos_width * config_dict['legend_rect_x']
	rect_y = circos_height * config_dict['legend_rect_y']

	y1 = rect_y + 10
	x1 = rect_x + 10	


	#--- 生成新的line3456的脚本，用于替换 
	line3_script = "<svg width=\"%spx\" height=\"%spx\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" % (new_circos_width,new_circos_height)
	line456_script = "<g id=\"bg\">\n<rect x=\"0\" y=\"0\" width=\"%spx\" height=\"%spx\" style=\"fill:rgb(255,255,255);\"/>\n</g>\n" % (new_circos_width,new_circos_height)




		
	for key in sorted(config_dict.keys()):
	
	
		if re.match(r'^legend(\d+)$',key):

			#print("%s\t%s" % (key,config_dict[key]))

			x1 = rect_x + 10
			
			if 'type' not in config_dict[key]:

				sys.exit("legend %s 没有设置 type 类型，die!\n" % key)

			if 'name' not in config_dict[key]:
				sys.exit("lenged %s 没有设置 legend name,die!\n" % key)
			if 'color' not in config_dict[key]:
				sys.exit("legend %s 没有设置颜色,die!\n" % key)

			plot_type = config_dict[key]['type']
			plot_fill_color = config_dict[key]['color']
			plot_legend_txt = config_dict[key]["name"]
			
			


			#开始生成新的 svg 
			if plot_type == "highlight":
				x2 = x1
				y2 = y1+60
				
				line_script1 = "<line x1=\"%s\" y1=\"%s\" x2=\"%s\" y2=\"%s\" style=\"stroke:%s;stroke-width:2\"/>\n" % (x1,y1,x2,y2,plot_fill_color)
				line_script2 = ""	
				xxx = 1
				while xxx <= 4:
					x1 = x1 + 10
					x2 = x1
					line_script2 = "%s<line x1=\"%s\" y1=\"%s\" x2=\"%s\" y2=\"%s\" style=\"stroke:%s;stroke-width:2\"/>\n" % (line_script2,x1,y1,x2,y2,plot_fill_color)	
					xxx = xxx + 1
				
				t_x = x2 + 20
				t_y = y2 - 18
				txt = "<text x=\"%s\" y=\"%s\" font-size=\"35\" fill=\"%s\">%s</text>\n" % (t_x,t_y,plot_fill_color,plot_legend_txt)
				script = "%s %s %s %s" % (script,line_script1,line_script2,txt)					
			
				y1 = y1 + 70
				tmp_count = tmp_count + 1

			elif plot_type == "histogram":
				xx = x1
				yy = y1
				y2 = y1 + 60
				
				rect_script1 = "<rect x=\"%s\" y=\"%s\" width=\"15\" height=\"60\" style=\"fill:%s;stroke:black;stroke-width:1;fill-opacity:1; stroke-opacity:0\"/>\n" % (xx,yy,plot_fill_color)
				xx = xx + 15
				rect_script2 = "<rect x=\"%s\" y=\"%s\" width=\"20\" height=\"10\" style=\"fill:%s;stroke:black;stroke-width:1;fill-opacity:1; stroke-opacity:0\"/>\n" % (xx,yy,plot_fill_color) 				
				xx = xx + 20
				rect_script3 = "<rect x=\"%s\" y=\"%s\" width=\"10\" height=\"40\" style=\"fill:%s;stroke:black;stroke-width:1;fill-opacity:1; stroke-opacity:0\"/>\n" % (xx,yy,plot_fill_color)
			

				t_x = xx + 20
				t_y = y2 - 18
				txt = "<text x=\"%s\" y=\"%s\" font-size=\"35\" fill=\"%s\">%s</text>\n" % (t_x,t_y,plot_fill_color,plot_legend_txt)
			
				script = "%s %s %s %s %s" % (script,rect_script1,rect_script2,rect_script3,txt)	
				y1 = y1 + 70

				tmp_count = tmp_count + 1
				
				
			elif plot_type == "ideogram":
				xx = x1
				yy = y1 + 15
				script = "%s<rect x=\"%s\" y=\"%s\" width=\"50\" height=\"30\" style=\"fill:%s;stroke:%s;stroke-width:1;fill-opacity:1; stroke-opacity:0\"/>\n" % (script,xx,yy,plot_fill_color,plot_fill_color)
				
				t_x = xx + 60
				t_y = yy + 25
				script = "%s<text x=\"%s\" y=\"%s\" font-size=\"35\" fill=\"%s\">%s</text>\n" % (script,t_x,t_y,plot_fill_color,plot_legend_txt)		
		
				tmp_count = 1 + tmp_count
				y1 = y1 + 70
			
				
				
	rect_height = 10 + (70 * tmp_count)
	rect_script = "<rect x=\"%s\" y=\"%s\" width=\"%s\" height=\"%s\" style=\"stroke:black;stroke-width:5; fill-opacity:0;stroke-opacity:0.1\"/>\n" % (rect_x,rect_y,rect_width,rect_height)
	script = "%s %s %s %s" % (line3_script,line456_script,rect_script,script)
	print(script)		
				
				
			
	outsvg = "%s/%s.svg" % (outdir,prefix)

	os.system("sed -i '3d' %s" % outsvg)
	os.system("sed -i '3d' %s" % outsvg)
	os.system("sed -i '3d' %s" % outsvg)
	os.system("sed -i '3d' %s" % outsvg)
	

	tmpfile = "%s/%s_tmp.svg" % (outdir,prefix)
	tp = open(tmpfile,'w+')
	tp.write(script)
	tp.close()
	
	os.system("sed -i '/Graphics\/SVG\/1.1\/DTD\/svg11.dtd\">/r %s' %s" % (tmpfile,outsvg))

	os.system("rm %s" % tmpfile)



if __name__ == '__main__':

	import os,sys
	
	args = p.parse_args()
	if args.insvg is None or args.outputdir is None or args.prefix is None or args.conf is None:
		os.system("python %s -h" % os.path.abspath(sys.argv[0]))
		sys.exit()
		

	
	run(args.insvg,args.outputdir,args.prefix,args.conf)	

	





