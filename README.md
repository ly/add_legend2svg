这是一个独立于circos软件的脚本，用于给没有图例的svg脚本添加图例。如circos软件生成的svg文件。

目前针对于 circos v0.69.6 版本的output，如果生成的 svg的3-6行，与下面的格式一样（定义的颜色、width、height可以不一致），同样也可以使用：

#---第3--6行svg start--- 

<svg width="3000px" height="3000px" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<g id="bg">

<rect x="0" y="0" width="3000px" height="3000px" style="fill:rgb(255,255,255);"/>

</g>

#---第3--6行svg end--- 

具体使用方法见 python add_legend2svg.py -h


##---快速开始---

查看 test.sh 配置 config 文件，如有问题联系 496187474@qq.com



##---配置文件说明---

#--- 配置文件格式 --  
{

	"legend_rect_x":0.87,
	
	"legend_rect_y":0.8,
	
	"rect_width":0.3,
	
	"new_svg_width":1.31,
	
	"new_svg_height":1.1,
	
	
	"legend1":{

		"name":"Genes(The Second Circle)",
	
		"type":"highlight",
	
		"color":"rgb(220,20,60)"
	
	},
	
	"legend2":{

		"name":"CARD's Gene(The Fourth Circle)",
	
		"type":"highlight",
	
		"color":"rgb(138,43,226)"

	},

	"legend3":{

		"name":"GC-Content(The Seventh Circle)",

		"type":"histogram",

		"color":"rgb(128,128,0)"

	},

	"legend4":{

		"name":"Contigs(The First Circle)",

		"type":"ideogram",

		"color":"rgb(0,0,0)"

	},

	"legend5":{

		"name":"VFDB's Gene(The Third Circle)",

		"type":"highlight",

		"color":"rgb(255,182,193)"

	},

	"legend6":{

		"name":"tRNA(The Fifth Circle)",

		"type":"highlight",

		"color":"rgb(30,144,255)"

	},

	"legend7":{

		"name":"rRNA(The Sixth Circle)",

        "type":"highlight",
                
        "color":"rgb(255,99,71)"

	}

}

#--- 配置文件解释 ---

配置文件按照JSON格式书写

legend_rect_x : 指添加的图例的位置x坐标，实际值 = value * 原svg的宽度值

legend_rect_y : 指添加的图例的位置y坐标，实际值 = value * 原svg的高度值

rect_width :    新增图例的宽度， 实际值 = value * 原svg宽度（高度不用设定，根据添加的layer自动增加）

new_svg_width : 添加图例后，svg的宽度，实际值 = value * 原svg宽度值

new_svg_height: 添加图例后，svg的高度，实际值 = value * 原svg高度值


其余的配置，都是添加图例的具体信息，如：
        
    "legend1":{

		"name":"Genes(The Second Circle)",
		"type":"highlight",
		"color":"rgb(220,20,60)"
	}
	
	legend1表示标记的名字（按照assic排序，小的放在图例的最上面，但必须是以lenend开始，后面接数字）
	
	name表示在图例上展示的文字说明
	
	type表示图例上图形，目前只支持 highlight、histogram、ideogram，后续会逐步添加 line、heatmap等

    color表示图例的颜色，需要与cicros里面的颜色保持一致（由于svg不支持circos里面的颜色，在配置circos和添加图例config时候，请直接使用 rgb）

